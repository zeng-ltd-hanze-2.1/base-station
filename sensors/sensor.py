import datetime
import os
import json
from pathlib import Path

# Base sensor class
class Sensor:

    # Constructor
    def __init__(self, sensor_type):
        self.sensor_type = sensor_type
        self.values = []
        self.gui_event = None

        # Get readings from storage
        self.get_readings()

        # Cleanup old readings
        # self.cleanup_saved_readings()

    # Get sensor storage file location
    def get_file_location(self):
        path = os.path.dirname(__file__)
        filename = os.path.join(path, '../readings/{}.json'.format(self.sensor_type))
        return filename

    # Get readings from file
    def get_readings(self):
        try:
            # Open readings file
            with open(self.get_file_location()) as data_file:
                self.values = json.load(data_file)
        except:
            self.values = []

    # Store updated readings
    def store_readings(self):
        # Create file if not exists
        data_file = Path(self.get_file_location())
        if not data_file.is_file():
            open(self.get_file_location(), 'a').close()

        # Write data to json file
        with open(self.get_file_location(), 'w') as data_file:
            json.dump(self.values, data_file)


    # Add value new value to values array
    def add_new_value(self, value):
        print(self.sensor_type)
        print(value)
        # Set new value to values array with datetime stamp
        self.values.append([datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") ,value])

        # Save values
        self.store_readings()

        # Check event
        if not self.gui_event is None:
            self.gui_event(self)

    # Get last known value
    def get_last_value(self):
        self.get_readings()

        if len(self.values) > 0:
            return self.values[-1]
        else:
            return 0

    # Get all values
    def get_values(self):
        return self.values

    # Get last number of values
    def get_last_amount_values(self, limit):
        return self.values

    # Cleanup saved readings
    def cleanup_saved_readings(self):
        self.values = self.get_last_amount_values(100)
        self.store_readings()

    # Add gui event
    def add_gui_event(self, event):
        self.gui_event = event
