from sensors.sensor import Sensor

# Define temperature sensor class
class Temperature(Sensor):

    # Class constructor
    def __init__(self):
        # Init sensor class
        super().__init__('temperature')

    # Convert value to actual temperature value
    def add_new_value(self, value):
        # Convert to int
        value = int(value, 16)

        # Convert value to temperature
        value *= 5
        value /= 1024
        value -= 0.5
        value *= 100

        # Add new value
        super().add_new_value(value)
