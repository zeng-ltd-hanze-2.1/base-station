from sensors.sensor import Sensor

# Define status sensor class
class Status(Sensor):

    # Class constructor
    def __init__(self):
        super().__init__('status')

    # Convert value to readable status
    def add_new_value(self, value):
        # Convert to int
        value = int(value, 16)


        # Convert value to temperature
        if value > 0 and value < 5:
            value = [
                "top",
                "bottom",
                "moving",
                "stop",
                "unknown"
            ][value]

            # Add new value
            super().add_new_value(value)
