from sensors.sensor import Sensor

# Define light sensor class
class Light(Sensor):

    # Class constructor
    def __init__(self):
        # Init sensor class
        super().__init__('light')

    # Convert value to actual light percentage
    def add_new_value(self, value):
        # Convert to int
        value = int(value, 16)

        # Convert value to temperature
        value = value / 1023 * 100

        # Add new value
        super().add_new_value(value)
