from sensors.sensor import Sensor

# Define distance sensor class
class Distance(Sensor):

    # Class constructor
    def __init__(self):
        # Init sensor class
        super().__init__('distance')

    # Convert value to actual distance
    def add_new_value(self, value):
        # Convert to int
        value = int(value, 16)

        # Convert valuu
        value = value / 4

        # Add new value
        super().add_new_value(value)
