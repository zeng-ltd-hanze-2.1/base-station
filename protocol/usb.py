import serial
import time
from multiprocessing import Process

# Define protocol class
class USBProtocol():

    # Class constructor
    def __init__(self, com_port, baud_rate):
        # Set self varibales
        self.com_port = com_port
        self.baud_rate = baud_rate
        self.connected = False
        self.serial = None

        # Define receiving history
        self.history = []

        # Define empty events
        self.events = {"temperature": None,
            "light": None,
            "ultrasonic": None,
            "screen-status": None,
            "ignore": None};
        self.listen_events = {1: "screen-status",
            2: "temperature",
            4: "light",
            8: "ultrasonic",
            16: "ignore"}

    # Create serial connection
    def create_connection(self):
        try:
            # Create serial connection
            self.serial = serial.Serial(self.com_port,
                self.baud_rate)
            self.connected = True
        except:
            self.connected = False

    # Event handler
    def event_handler(self):
        # Check history length
        if len(self.history) == 3:
            # Get event
            event = self.history[0]
            # Get 10 bits of data
            value1 = self.history[1]
            value2 = self.history[2]

            # Convert hexidecimal data to binary
            bin_val_1 = bin(int(value1, 16))[2:].zfill(8)
            bin_val_2 = bin(int(value2, 16))[2:].zfill(8)

            # Convert back to hexidecimal
            bin_value = "{}{}".format(bin_val_2, bin_val_1[0:-6])
            value = hex(int(bin_value, 2))

            # Run event with data
            if int(event, 16) in self.listen_events:
                self.events[self.listen_events[int(event, 16)]](value)

            # Reset history
            self.history = []
        elif len(self.history) > 3:
            self.history = []

    # Add event handler
    def add_event_handler(self, event, function):
        self.events[event] = function

    # Setup listener
    def setup_listener(self):
        # Create listen proces
        process = Process(target = self.listen)
        process.start()

    # Listen to serial port
    def listen(self):
        # Infinite while
        while 1:
            # Check if connected
            if not self.connected:
                self.create_connection()

            try:
                if self.serial is None:
                    # Not connected
                    self.connected = False
                    time.sleep(1)
                else:
                    # Get data
                    data = self.serial.read()

                    # Add data
                    self.history.append(data.hex())
                    # Call event handler
                    self.event_handler()
            except:
                # Not connected
                self.connected = False
                time.sleep(1)

    # Write to serial port
    def write(self, data):
        # Use serial
        with self.serial as serial:
            # Write data
            serial.write(data)
