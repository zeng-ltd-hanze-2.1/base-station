import tkinter as tk
from tkinter import ttk
from random import randrange
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

class BrightnessGraphPage(tk.Frame):

    def __init__(self, parent, controller, light):
        self.light = light
        self.trigger = 0
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Light Graph", font=("Verdana", 12))
        label.pack(pady=10,padx=10)

        button1 = ttk.Button(self, text="Back to Status Sreen", command=lambda: controller.show_frame(0))
        button1.pack()

        self.f = Figure(figsize=(5,5), dpi=100)
        self.a = self.f.add_subplot(111)

        self.plot()

        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(expand=True)

        self.toolbar = NavigationToolbar2TkAgg(self.canvas, self)
        self.toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    def plot(self):
        # Get temp values and create arrays
        xValues = []
        yValues = []
        
        for tempValues in self.light.get_last_amount_values(100):
            if not type(tempValues) is float:
                xValues.append(len(xValues) + 1)
                yValues.append(tempValues[1])

        self.a.plot(xValues, yValues)
