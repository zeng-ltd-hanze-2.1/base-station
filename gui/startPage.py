import tkinter as tk
from tkinter import ttk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.label1 = tk.Label(self, text="Status Screen", font=("Verdana", 12))
        self.label1.place(relx=0.4)

        #Screen brightness status and button
        self.label2 = tk.Label(self, text="The screen is currently: ")
        self.label2.place(relx=0.05, rely=0.10)

        self.label3 = tk.Label(self, text="The current brightness is 0%")
        self.label3.place(relx=0.05, rely=0.15)

        self.button2 = ttk.Button(self, text="Visit brightness graph page", command=lambda: controller.show_frame(1))
        self.button2.place(relx=0.6, rely=0.15)

        #Screen temperature status and button
        self.label4 = tk.Label(self, text="The current temperature is 0C")
        self.label4.place(relx=0.05, rely=0.2)

        self.button3 = ttk.Button(self, text="Visit temperature graph page.", command=lambda: controller.show_frame(2))
        self.button3.place(relx=0.6, rely=0.2)

        #Screen temperature status and button
        self.label5 = tk.Label(self, text="The screen has a height of 0 cm")
        self.label5.place(relx=0.05, rely=0.25)


    def get_screen_height(self):
        dict = {}
        dict[0] = "lowered"
        dict[1] = "half raised"
        dict[2] = "raised"

        value = 0
        return dict[value]

    def update_frame(self, temperatureSensor, lightSensor, statusSensor, distanceSensor):
        light = lightSensor.get_last_value()
        temp = temperatureSensor.get_last_value()
        distance = distanceSensor.get_last_value()
        status = statusSensor.get_last_value()

        if not type(light) is int and not type(temp) is int and not type(distance) is int:
            self.label2 = tk.Label(self, text="The screen is currently: %s" % status[1])
            self.label2.place(relx=0.05, rely=0.10)

            # Update labels
            self.label3 = tk.Label(self, text="The current brightness is %s%%" % round(light[1], 1))
            self.label3.place(relx=0.05, rely=0.15)

            self.label4 = tk.Label(self, text="The current temperature is %s C" % round(temp[1], 1))
            self.label4.place(relx=0.05, rely=0.2)

            self.label5 = tk.Label(self, text="The screen has a height of %s cm" % round(distance[1], 1))
            self.label5.place(relx=0.05, rely=0.25)
