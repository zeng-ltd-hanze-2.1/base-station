import tkinter as tk
from tkinter import ttk
from random import randrange
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

from gui.startPage import StartPage
from gui.lightGraph import BrightnessGraphPage
from gui.temperatureGraph import TemperatureGraphPage

class GUI(tk.Tk):

    # Class constructor
    def __init__(self, temperatureSensor, lightSensor, statusSensor, distanceSensor, *args, **kwargs):

        # Define sensor classes
        self.temperatureSensor = temperatureSensor
        self.lightSensor = lightSensor
        self.statusSensor = statusSensor
        self.distanceSensor = distanceSensor

        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.wm_title(self, "Base Station")

        container = tk.Frame(self)
        container.pack()

        self.frames = {}

        self.frames = {}
        index = 0

        frame = StartPage(container, self)

        self.frames[index] = frame

        frame.grid(row=0, column=0, sticky="nsew")
        index += 1

        frame = BrightnessGraphPage(container, self, lightSensor)

        self.frames[index] = frame

        frame.grid(row=0, column=0, sticky="nsew")
        index += 1

        frame = TemperatureGraphPage(container, self, temperatureSensor)

        self.frames[index] = frame

        frame.grid(row=0, column=0, sticky="nsew")
        index += 1

        self.show_frame(0)

    def show_frame(self, index):
        frame = self.frames[index]
        frame.tkraise()

    # Sensor events
    def update_start_page(self, temperatureSensor, lightSensor, statusSensor, distanceSensor):
        self.frames[0].update_frame(temperatureSensor, lightSensor, statusSensor, distanceSensor)
        self.frames[1].plot()
        self.frames[2].plot()
