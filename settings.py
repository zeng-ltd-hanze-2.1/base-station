# Import dotenv
from dotenv import load_dotenv, find_dotenv

# Load env variables
load_dotenv(find_dotenv())
