'''
Base station code for project 2.1

This is the main file of this program that calls
all init functions of the project elements
'''

# Imports
from protocol.usb import USBProtocol
from sensors.temperature import Temperature
from sensors.light import Light
from sensors.status import Status
from sensors.distance import Distance
from gui.gui import GUI
import settings
import os

def program_init():
    # Init usb protocol
    usb = USBProtocol(os.environ.get("COM_PORT"), os.environ.get("BAUD_RATE"))

    # Init sensors
    temperatureSensor = Temperature()
    lightSensor = Light()
    statusSensor = Status()
    distanceSensor = Distance()

    # Add event handlers
    usb.add_event_handler("screen-status", statusSensor.add_new_value)
    usb.add_event_handler("temperature", temperatureSensor.add_new_value)
    usb.add_event_handler("light", lightSensor.add_new_value)
    usb.add_event_handler("ultrasonic", distanceSensor.add_new_value)

    # Init GUI
    gui = GUI(temperatureSensor, lightSensor, statusSensor, distanceSensor)

    # Setup usb listener
    usb.setup_listener()
    # Loop gui
    def update():
        gui.update_start_page(temperatureSensor, lightSensor, statusSensor, distanceSensor)
        gui.after(1000, update)

    gui.after(1000, update)
    gui.mainloop()

program_init()
